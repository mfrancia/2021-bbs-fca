# Sistemi Informativi - Lab

Link: http://bitbucket.org/mfrancia/2021-bbs-fca/

### Whoami?

Matteo Francia (m.francia@unibo.it)

### Corso

Bologna Business School, BBS.

Bologna Fiere, Padiglione 15. Ingresso sito in Via Alfieri Maserati 13/9, 40128, Bologna (BO). 

Riferimento: Paolo Onnis <paolo.onnis@bbs.unibo.it>

Riferimento lab: Marco Natali <marco.natali@bbs.unibo.it>

#### Day 1 (3h) - 16/02

- Intro to Access

#### Day 2 (3h) - 02/03

- Intro to Tableau